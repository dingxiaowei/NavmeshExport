﻿using UnityEditor;
using UnityEditor.SceneManagement;

class SceneEditor
{
    [MenuItem("Tools/Bake Scenes In Build")]
    private static void Bake()
    {
        var activeScene = EditorSceneManager.GetActiveScene().path;

        foreach (var scene in EditorBuildSettings.scenes)
        {
            if (!scene.enabled)
                continue;

            EditorSceneManager.OpenScene(scene.path, OpenSceneMode.Single);
            Lightmapping.Bake();

            EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
        }

        if (activeScene != "")
            EditorSceneManager.OpenScene(activeScene);
    }
}